package person.search

import person.types.Person
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import java.util.concurrent.TimeUnit

class SearchRepository(private val searchView: SearchView, private val personBackend: PersonBackend) {


    fun search(): Observable<List<Person>> {

        /**
         * Implement the search call according the following criteria:
         * - The search query string must contain at least 3 characters
         * - To save backend traffic: only search if search query hasn't changed within the last 300 ms
         * - If the user is typing fast "Hannes" and than deletes and types "Hannes" again (exceeding 300 ms) the search should not execute twice.
         */


        return Observable.create { observer ->
            searchView.onSearchTextchanged()
                .debounce(300,  TimeUnit.MILLISECONDS)
                .filter { text -> text.length >= 3 }
                .distinctUntilChanged()
                .subscribeBy (
                        onNext = { text ->
                            personBackend.searchfor(text)
                                .filter { searchRes -> searchRes.isNotEmpty()}
                                .subscribe { searchRes -> observer.onNext(searchRes) }
                        },
                        onComplete = { observer.onComplete() }
                )
        }
    }
}
