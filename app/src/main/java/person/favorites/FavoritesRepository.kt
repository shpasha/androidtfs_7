package person.favorites

import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import person.types.PersonWithAddress

internal class FavoritesRepository(private val personBackend: PersonBackend, private val favoritesDatabase: FavoritesDatabase) {


    fun loadFavorites(): Observable<List<PersonWithAddress>> {

        /**
         * Provide an observable that only emits a list of PersonWithAddress if they are marked as favorite ones.
         */

        return Observable.create{ observer ->
            personBackend.loadAllPersons().subscribe { personsWithAddressList ->
                favoritesDatabase.favoriteContacts().subscribeBy(
                    onNext = { favs ->
                        val result = mutableListOf<PersonWithAddress>()
                        favs.forEach {favId ->
                            personsWithAddressList.forEach{
                                if (it.person.id == favId) result.add(it)
                            }
                        }
                        observer.onNext(result)
                    },
                    onComplete = {
                        observer.onComplete()
                    }
                )
            }
        }

    }
}
