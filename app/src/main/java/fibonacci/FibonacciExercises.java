package fibonacci;

import io.reactivex.Observable;

public class FibonacciExercises {

	public Observable<Integer> fibonacci(int n) {
		return Observable.create(observer -> {
            int[] f = new int[n + 2];
            f[0] = 0; f[1] = 1;
            for (int i = 0; i < n; i++) {
                observer.onNext(f[i]);
                f[i + 2] = f[i + 1] + f[i];
            }
            observer.onComplete();
        });
	}
}
